package com.example.Evaluation_FAQ.web.question;

import com.example.Evaluation_FAQ.application.QuestionService;
import com.example.Evaluation_FAQ.domain.model.question.Question;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/questions")
public class QuestionController {

    private final QuestionService questionService;

    public QuestionController(QuestionService questionService){
        this.questionService=questionService;
    }

    // Création d'une question
    @PostMapping
    ResponseEntity<Question> createQuestion(@RequestBody CreateQuestionRequestDto body){
        Question createdQuestion = questionService.createQuestion(body.title(), body.content());
        return ResponseEntity.status(HttpStatus.CREATED).body(createdQuestion);
    }

    // Retrouver une question par son ID
    // A revoir
    @GetMapping("/{questionId}")
    ResponseEntity<Question> findQuestion(@PathVariable String questionId){
        return questionService.findById(questionId).map(u -> ResponseEntity.ok(u)).orElseGet(() -> ResponseEntity.notFound().build());
    }

    // Récupération de toutes les questions
    @GetMapping
    ResponseEntity<List<Question>> findAllQuestion(){
        return ResponseEntity.ok(questionService.findAll());
    }

    // Modification d'une question
    @PutMapping
    ResponseEntity<Question> editQuestion (@PathVariable String questionId, @RequestBody UpdateQuestionDto body){
        Question editedQuestion = questionService.updateQuestion(questionId, body.title(), body.content());
        return ResponseEntity.status(HttpStatus.OK).body(editedQuestion);
    }

    // Effacer une question
    @DeleteMapping("/{questionId}")
    @ResponseStatus(HttpStatus.OK)
    void deleteQuetion(@PathVariable String questionId){
        questionService.deleteQuestion(questionId);
    }
}
