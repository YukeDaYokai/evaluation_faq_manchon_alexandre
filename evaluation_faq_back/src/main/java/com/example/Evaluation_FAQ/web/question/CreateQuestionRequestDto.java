package com.example.Evaluation_FAQ.web.question;

public record CreateQuestionRequestDto (String title, String content){
}
