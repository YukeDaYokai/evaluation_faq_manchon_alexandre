CREATE TABLE IF NOT EXISTS questions(
    id char(36) primary key,
    title text not null,
    content text not null,
    dateTime TIMESTAMP
);

CREATE TABLE IF NOT EXISTS answers(
    id char(36) primary key,
    content text not null,
    dateTime TIMESTAMP,
    question_id char(36) REFERENCES questions (id)
);