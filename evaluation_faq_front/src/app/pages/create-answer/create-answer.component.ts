import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Answer } from 'src/app/model/answer.model';
import { AnswerService } from 'src/app/services/answer.service';

@Component({
  selector: 'app-create-answer',
  templateUrl: './create-answer.component.html',
  styleUrls: ['./create-answer.component.scss']
})
export class CreateAnswerComponent implements OnInit {

  public content: string ='';

  public answers: Answer[]=[];

  public id: string='';

  constructor(public answerService: AnswerService, private route:Router, private router:ActivatedRoute) { }

  async ngOnInit(){
    this.id=this.router.snapshot.params["id"]
    this.answers = await this.answerService.fetch(this.id)
  }

  createAnswer(){
    this.answerService.createAnswer(this.content, this.id);
    this.route.navigate(["/"])
  }
}
