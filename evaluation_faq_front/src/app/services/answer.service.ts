import { Injectable } from '@angular/core';
import axios from 'axios';
import { Answer } from '../model/answer.model';

@Injectable({
  providedIn: 'root'
})
export class AnswerService {

  private apiUrl: string='http://localhost:8080/questions/answers';

  public async fetch(id: string): Promise <Answer[]>{
    const{data}=await axios.get(`${this.apiUrl}/${id}`);
    console.log(data)
    return data.map((answer: any)=>{
      return{
        id: answer.id,
        content: answer.content
      }
    });
  }

  public createAnswer(content: string, id:string){
    const apiUrl='http://localhost:8080/questions/answers';
    axios.post(`${apiUrl}/${id}`,{content})
    .then((res)=>{
      console.log(res.data);
    })
    .catch((err)=>{
      console.log(err);
    });
  }

  constructor() { }
}
