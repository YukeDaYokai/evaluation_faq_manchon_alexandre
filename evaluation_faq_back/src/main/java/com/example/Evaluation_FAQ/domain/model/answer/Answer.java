package com.example.Evaluation_FAQ.domain.model.answer;

import com.example.Evaluation_FAQ.domain.model.question.Question;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name="answers")
@Access(AccessType.FIELD)
public class Answer {

    @Id
    private String id;
    private String content;
    private LocalDateTime datetime;

    @ManyToOne
    @JoinColumn(name="question_id")
    private Question question;

    protected Answer(){
        // FOR JPA
    }

    public Answer(String id, String content, LocalDateTime datetime, Question question){
        this.id=id;
        this.content=content;
        this.datetime=datetime;
        this.question=question;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public LocalDateTime getDatetime() {
        return datetime;
    }

    public void setDatetime(LocalDateTime datetime) {
        this.datetime = datetime;
    }
}
