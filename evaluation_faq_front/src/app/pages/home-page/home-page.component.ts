import { Component, OnInit } from '@angular/core';
import { Question } from 'src/app/model/question.model';
import { QuestionService } from 'src/app/services/question.service';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {

  // On crée un tableau de questions pour les stocker
  public questions:Question[]=[];

  constructor(public questionService: QuestionService) { }

  async ngOnInit() {
    this.questions = await this.questionService.fetch()
  }

}
