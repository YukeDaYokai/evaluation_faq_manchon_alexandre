package com.example.Evaluation_FAQ.domain.model.question;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name="questions")
@Access(AccessType.FIELD)
public class Question {

    @Id
    private String id;
    private String title;
    private String content;
    private LocalDateTime datetime;
   // private Date date = new Date(new Date().getTime());

    protected Question(){
        // FOR JPA
    }

    public Question(String id, String title, String content, LocalDateTime datetime){
        this.id=id;
        this.title=title;
        this.content=content;
        this.datetime = datetime;
    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getContent() {
        return content;
    }

    public LocalDateTime getDatetime() {
        return datetime;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setDatetime(LocalDateTime datetime) {
        this.datetime = datetime;
    }
}
