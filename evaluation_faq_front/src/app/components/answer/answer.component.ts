import { Component, Input, OnInit } from '@angular/core';
import { Answer } from 'src/app/model/answer.model';
import { AnswerService } from 'src/app/services/answer.service';

@Component({
  selector: 'app-answer',
  templateUrl: './answer.component.html',
  styleUrls: ['./answer.component.scss']
})
export class AnswerComponent implements OnInit {

  @Input()
  public answer?: Answer;

  constructor(public answerService: AnswerService) { }

  ngOnInit(): void {
  }

}
