package com.example.Evaluation_FAQ.domain.model.repository;

import com.example.Evaluation_FAQ.domain.model.question.Question;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface QuestionRepository extends CrudRepository<Question, String> {
}
