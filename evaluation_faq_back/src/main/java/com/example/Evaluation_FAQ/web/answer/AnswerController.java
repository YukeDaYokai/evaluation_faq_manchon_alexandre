package com.example.Evaluation_FAQ.web.answer;

import com.example.Evaluation_FAQ.application.AnswerService;
import com.example.Evaluation_FAQ.domain.model.answer.Answer;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/questions/answers")
public class AnswerController {

    private final AnswerService answerService;

    public AnswerController(AnswerService answerService){
        this.answerService=answerService;
    }

    // Création d'une réponse
    @PostMapping("/{questionId}")
    ResponseEntity<Answer> createAnswer(@RequestBody CreateAnswerRequestDto body, @PathVariable String questionId){
        Answer createdAnswer = answerService.createAnswer(body.content(), questionId);
        return ResponseEntity.status(HttpStatus.CREATED).body(createdAnswer);
    }

    // Retrouver une réponse par son ID
//    @GetMapping
//    ResponseEntity<Answer> findAnswer(@PathVariable String answerId){
//        return answerService.findById(answerId).map(u -> ResponseEntity.ok(u)).orElseGet(() -> ResponseEntity.notFound().build());
//    }

    // Récupération de toutes les réponses TEST 1
//    @GetMapping("/{questionId}")
//    ResponseEntity<List<Answer>> findAllAnswer(@PathVariable String questionId){
//        return ResponseEntity.ok(answerService.getAnswers(questionId));
//    }

    // Récupération de toutes les réponses TEST 2
    @GetMapping("/{questionId}")
    public List<Answer> getAnswers(@PathVariable String questionId){
        return answerService.getAnswers(questionId);
    }

    // Modification d'une réponse
    @PutMapping
    ResponseEntity<Answer> editAnswer (@PathVariable String answerId, @RequestBody UpdateAnswerDto body){
        Answer editedAnswer = answerService.updateAnswer(answerId, body.content());
        return ResponseEntity.status(HttpStatus.OK).body(editedAnswer);
    }

    // Effacer une réponse
    @DeleteMapping("/{answerId}")
    @ResponseStatus(HttpStatus.OK)
    void deleteAnswer(@PathVariable String answerId){
        answerService.deleteAnswer(answerId);
    }
}
