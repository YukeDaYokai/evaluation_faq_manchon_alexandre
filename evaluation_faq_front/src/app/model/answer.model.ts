export interface Answer{
    id: string,
    content: string,
    datetime: Date
}