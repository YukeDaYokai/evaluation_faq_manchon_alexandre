package com.example.Evaluation_FAQ.domain.model.repository;

import com.example.Evaluation_FAQ.domain.model.answer.Answer;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface AnswerRepository extends CrudRepository <Answer, String> {

    public List<Answer> findByQuestionId (String questionId);
}
