package com.example.Evaluation_FAQ.web.answer;

public record CreateAnswerRequestDto(String content) {
}
