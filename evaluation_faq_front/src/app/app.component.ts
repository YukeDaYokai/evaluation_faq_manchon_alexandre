import { Component } from '@angular/core';
import { authInterceptor } from './helpers/interceptor';
import { LoginService } from './services/login.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Evaluation_FAQ_front';

constructor(private loginService: LoginService){
  authInterceptor(this.loginService)
}

}