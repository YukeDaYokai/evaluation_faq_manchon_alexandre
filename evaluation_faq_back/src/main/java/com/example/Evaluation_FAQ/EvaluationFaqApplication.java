package com.example.Evaluation_FAQ;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EvaluationFaqApplication {

	public static void main(String[] args) {
		SpringApplication.run(EvaluationFaqApplication.class, args);
	}

}
