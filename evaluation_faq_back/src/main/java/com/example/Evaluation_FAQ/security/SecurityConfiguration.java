package com.example.Evaluation_FAQ.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class SecurityConfiguration {
    // On implémente une méthode pour gérer les droits des utilisateurs
    @Bean
    SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        return http
                .antMatcher("/**")
                .csrf().disable()
                .authorizeRequests(authorize->authorize
                        .antMatchers(HttpMethod.GET,"/questions/**").permitAll()
                        .antMatchers(HttpMethod.GET,"/answers").permitAll()
                        .antMatchers(HttpMethod.POST, "/users").permitAll()
                        .anyRequest().authenticated()
                )
                .httpBasic()
                .and()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .cors()
                .and()
                .build();
    }
    // En utilisant cette méthode sans paramétrer de User, par défaut il faut se connecter en temps que "user", avec le password généré au lancement de l'app.


    // Pour ne pas avoir de problème avec CrossOrigin, on implémente la méthode suivante :
    @Bean
    public WebMvcConfigurer corsConfigurer(){
        return new WebMvcConfigurer() {
            @Override
            public void addCorsMappings(CorsRegistry registry){
                registry.addMapping("/**").allowedOrigins("http://localhost:4200").allowedMethods("HEAD","GET","POST","PUT", "DELETE", "PATCH");
            }
        };
    }

    // Je ne sais pas encore comment gérer les utilisateurs autrement qu'en dur, donc en attendant :
    @Bean
    public UserDetailsService users(){
        UserDetails alexandre = User.builder().username("alexandre").password("{noop}zenikanar").roles("USER").build();
        return new InMemoryUserDetailsManager(alexandre);
    }

}
