package com.example.Evaluation_FAQ.web.question;

public record UpdateQuestionDto(String title, String content){
}
