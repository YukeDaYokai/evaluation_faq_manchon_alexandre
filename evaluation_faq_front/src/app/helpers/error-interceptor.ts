import { Router } from "@angular/router";
import axios from "axios";
import { LoginService } from "../services/login.service";

export const errorInterceptor = (loginService: LoginService, router: Router): void => {
    axios.interceptors.response.use(function(response){
        return response;
    }, function (error) {
        if(error.response.status === 401) {
            loginService.logout();
            router.navigate(["/loggin"]);
        }
        throw error;
    })
}