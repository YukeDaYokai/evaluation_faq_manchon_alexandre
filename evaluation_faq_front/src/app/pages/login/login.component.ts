import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from 'src/app/services/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  @Input()
  public username: string="";

  @Input()
  public password: string="";

  constructor(private route:Router, private loginService: LoginService) { }

  onSubmitLoggin(){
    this.loginService.login({username: this.username, password: this.password});
    this.loginService.verify()
    .then(()=>this.route.navigate(["/"]))
    .catch(()=>this.loginService.logout())
  }

  ngOnInit(): void {
  }

}
