import { Injectable } from '@angular/core';
import axios from 'axios';
import { Question } from '../model/question.model';

@Injectable({
  providedIn: 'root'
})
export class QuestionService {

  private apiUrl: string='http://localhost:8080/questions';

  public async fetch(): Promise <Question[]>{
    const{data}=await axios.get(`${this.apiUrl}`);
    console.log(data)
    return data.map((question: any)=>{
      return{
        id: question.id,
        title: question.title,
        content: question.content,
        datetime: question.datetime
      }
    });
  }

  // On crée une méthode pour rédiger une question, enregistrée dans notre BDD
  public createQuestion(title: string, content: string){
    const apiUrl='http://localhost:8080/questions';
    axios.post(`${apiUrl}`,{title, content})
    .then((res)=>{
      console.log(res.data);
    })
    .catch((err)=>{
      console.log(err);
    });
  }

  // On crée une méthode pour effacer une question
  public deleteQuestion (id: string){
    const apiUrl='http://localhost:8080/questions';
    axios.delete(`${apiUrl}/${id}`)
    .then((res)=>{
      console.log(res.data);
    })
    .catch((err)=>{
      console.log(err);
    });
  }

  constructor() { }
}
