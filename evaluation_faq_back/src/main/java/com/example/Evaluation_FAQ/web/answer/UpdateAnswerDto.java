package com.example.Evaluation_FAQ.web.answer;

import java.time.LocalDateTime;

public record UpdateAnswerDto(String id, String content) {
}
