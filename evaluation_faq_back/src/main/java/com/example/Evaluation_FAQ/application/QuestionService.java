package com.example.Evaluation_FAQ.application;

import com.example.Evaluation_FAQ.domain.model.question.Question;
import com.example.Evaluation_FAQ.domain.model.repository.QuestionRepository;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Component
public class QuestionService {

    private final QuestionRepository questionRepository;

    public QuestionService(QuestionRepository questionRepository){
        this.questionRepository=questionRepository;
    }

    // Implémentation d'une méthode pour créer une question
    public Question createQuestion(String title, String content){
        String[] words = title.split("");
        int numberOfWords = words.length;
        if (numberOfWords <= 20 && words[words.length - 1] .equals("?")){
            Question newQuestion = new Question(UUID.randomUUID().toString(), title, content, LocalDateTime.now().withNano(0));
            questionRepository.save(newQuestion);
            return newQuestion;
        }else{
            String error = "Le titre ne doit pas dépasser 50 mots et doit terminer par un '?'";
        }
        return null;
    }

    // Implémentation d'une méthode pour retrouver toutes nos questions
    public List<Question> findAll(){
        return(List<Question>) questionRepository.findAll();
    }

    // Implémentation d'une méthode pour retrouver une question par son id
    public Optional<Question> findById(String questionId){
        return questionRepository.findById(questionId);
    }

    // Implémentation d'une méthode pour éditer une question
    public Question updateQuestion (String questionId, String title, String content){
        Question questionToUpdate=questionRepository.findById(questionId).orElseThrow();
        questionToUpdate.setTitle(title);
        questionToUpdate.setContent(content);
        questionRepository.save(questionToUpdate);
        return questionToUpdate;
    }

    // Implémentation d'une méthode pour effacer une question par son id
    public void deleteQuestion(String questionId){
        questionRepository.deleteById(questionId);
    }
}
