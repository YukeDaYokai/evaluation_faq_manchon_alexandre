import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { CreateQuestionComponent } from './pages/create-question/create-question.component';
import { FormsModule } from '@angular/forms';
import { LoginComponent } from './pages/login/login.component';
import { UiHeaderComponent } from './pages/ui-header/ui-header.component';
import { QuestionComponent } from './components/question/question.component';
import { AnswerComponent } from './components/answer/answer.component';
import { CreateAnswerComponent } from './pages/create-answer/create-answer.component';

@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    CreateQuestionComponent,
    LoginComponent,
    UiHeaderComponent,
    QuestionComponent,
    AnswerComponent,
    CreateAnswerComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
