import { Injectable } from '@angular/core';
import axios from 'axios';
import { User } from '../model/user.model';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  private readonly SESSION_STORAGE_KEY = "currentUser";

  apiUrl: String = 'http://localhost:8080/users';

  public user?: User;

  constructor() { }

  verify(): Promise<any>{
    return axios.post(`${this.apiUrl}/me`);
  }

  login(user: User):void{
    sessionStorage.setItem(this.SESSION_STORAGE_KEY, JSON.stringify(user))
    console.log(user);
  }

  logout():void{
    sessionStorage.removeItem(this.SESSION_STORAGE_KEY)
  }

  getCurrentUserBasicAuthentication():string{
    const currentUserPlain = sessionStorage.getItem(this.SESSION_STORAGE_KEY)
    if (currentUserPlain){
      const currentUser = JSON.parse(currentUserPlain)
      return "Basic " + btoa(currentUser.username + ":" + currentUser.password);
    }else{
      return "";
    }
  }
}
