package com.example.Evaluation_FAQ.application;

import com.example.Evaluation_FAQ.domain.model.answer.Answer;
import com.example.Evaluation_FAQ.domain.model.repository.AnswerRepository;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Component
public class AnswerService {
    private final AnswerRepository answerRepository;
    private final QuestionService questionService;

    public AnswerService(AnswerRepository answerRepository, QuestionService questionService){
        this.answerRepository=answerRepository;
        this.questionService=questionService;
    }

    // Implémentation d'une méthode pour créer une réponse
    public Answer createAnswer(String content, String id){
        String[] words = content.split("");
        int numberOfWords = words.length;
        if (numberOfWords <= 100) {
            Answer newAnswer = new Answer(UUID.randomUUID().toString(), content, LocalDateTime.now().withNano(0), questionService.findById(id).orElse(null));
            answerRepository.save(newAnswer);
            return newAnswer;
        }else{
            String error = "La réponse ne doit pas dépasser 100 mots";
        }
        return null;
    }

    // Implémentation d'une méthode pour retrouver toutes nos réponses
    public List<Answer> findAll(String questionId){
        return(List<Answer>) answerRepository.findAll();
    }

    // Implémentation d'une méthode pour retrouver une réponse par son id
    public Optional<Answer> findById(String answerId){
        return answerRepository.findById(answerId);
    }

    // Implémentation d'une méthode pour éditer une réponse
    public Answer updateAnswer (String answerId,String content){
        Answer answerToUpdate=answerRepository.findById(answerId).orElseThrow();
        answerToUpdate.setContent(content);
        answerRepository.save(answerToUpdate);
        return answerToUpdate;
    }

    // Implémentation d'une méthode pour effacer une réponse par son id
    public void deleteAnswer(String answerId){
        answerRepository.deleteById(answerId);
    }

    public List<Answer> getAnswers (String questionId){
        return answerRepository.findByQuestionId(questionId);
    }
}
