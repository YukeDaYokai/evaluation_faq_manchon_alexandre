import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { QuestionService } from 'src/app/services/question.service';

@Component({
  selector: 'app-create-question',
  templateUrl: './create-question.component.html',
  styleUrls: ['./create-question.component.scss']
})
export class CreateQuestionComponent implements OnInit {

  public title: string ='';
  public content: string= '';

  constructor(public questionService: QuestionService, private route:Router) { }


  ngOnInit(): void {
  }

  createQuestion(){
    this.questionService.createQuestion(this.title, this.content);
    this.route.navigate(["/"])
  }
}
