import { Component, Input, OnInit} from '@angular/core';
import { Question } from 'src/app/model/question.model';
import { QuestionService } from 'src/app/services/question.service';

@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.scss']
})
export class QuestionComponent implements OnInit {

  @Input()
  public question?: Question;

  // OUTPUT A AJOUTER ?

  constructor(public questionService: QuestionService) { }


  async ngOnInit() {
  }

}
