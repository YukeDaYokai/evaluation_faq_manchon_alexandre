import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AnswerComponent } from './components/answer/answer.component';
import { CreateAnswerComponent } from './pages/create-answer/create-answer.component';
import { CreateQuestionComponent } from './pages/create-question/create-question.component';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { LoginComponent } from './pages/login/login.component';

const routes: Routes = [
  {path: '', component: HomePageComponent},
  {path: 'create-question', component: CreateQuestionComponent},
  {path: 'login', component: LoginComponent},
  // {path: 'answer', component: AnswerComponent},
  {path: 'create-answer/:id', component: CreateAnswerComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
